package irc

import (
	"bufio"
	"io"
)

func ReadIrc(done <-chan interface{}, reader io.Reader) <-chan string {
	messages := make(chan string)
	newreader := bufio.NewReader(reader)

	go func() {
		defer close(messages)

		for {
			select {
			case <-done:
				return
			default:
				res, _ := newreader.ReadString('\n')
				messages <- res
			}

		}

	}()

	return messages
}
