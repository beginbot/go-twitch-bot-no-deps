package irc

import (
	"fmt"
	"gitlab.com/beginbot/stylist/config"
)

func SendMsg(c *config.Config, msg string) {
	fmt.Fprintf(c.Conn, "PRIVMSG #%s :%s\r\n", c.Channel, msg)
}
