package main

import (
	"fmt"
	"gitlab.com/beginbot/stylist/config"
	"gitlab.com/beginbot/stylist/executor"
	"gitlab.com/beginbot/stylist/filter"
	"gitlab.com/beginbot/stylist/irc"
	"gitlab.com/beginbot/stylist/reporter"
	"gitlab.com/beginbot/stylist/router"
)

func main() {
	fmt.Println("Time to Style Things")
	c := config.NewConfig()

	done := make(chan interface{})
	messages := irc.ReadIrc(done, c.Conn)
	userMsgs := filter.Filter(done, messages)
	commands, _ := router.Router(done, userMsgs)
	results := executor.Execute(done, commands)
	reporter.Report(done, results, &c)

	for {
	}
}
