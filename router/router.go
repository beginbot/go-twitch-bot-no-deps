package router

import (
	"gitlab.com/beginbot/stylist/parser"
)

func Router(done <-chan interface{}, messages <-chan string) (<-chan string, <-chan string) {
	commands := make(chan string, 10)
	chatter := make(chan string, 10)

	go func() {
		defer close(commands)
		defer close(chatter)

		for msg := range messages {
			select {
			case <-done:
				return
			default:

				if parser.IsPrivmsg(msg) {
					_, m := parser.ParsePrivmsg(msg)
					if parser.IsCommand(m) {
						commands <- msg
					}
				} else {
					chatter <- msg
				}
			}
		}
	}()

	return commands, chatter

}

func Filter(done <-chan interface{}, messages <-chan string) <-chan string {
	filterChan := make(chan string)

	go func() {
		defer close(filterChan)
		for v := range messages {
			select {
			case <-done:
				return
			default:
				if parser.IsPrivmsg(v) {
					filterChan <- v
				}
			}
		}
	}()

	return filterChan
}
