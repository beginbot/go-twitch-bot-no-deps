package parser

import "strings"

func ParsePrivmsg(msg string) (string, string) {
	args := strings.Split(msg, " ")
	user := strings.Split(args[0], "!")[0][1:]
	user_msg := strings.Join(args[3:], " ")[1:]
	return user, user_msg
}

func IsPrivmsg(msg string) bool {
	return strings.Contains(msg, "PRIVMSG")
}

func IsPing(msg string) bool {
	return strings.Contains(msg, "PING")
}

func IsCommand(msg string) bool {
	return string(msg[0]) == "!"
}
