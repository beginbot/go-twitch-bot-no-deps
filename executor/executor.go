package executor

import (
	"fmt"
	"gitlab.com/beginbot/stylist/parser"
	"os/exec"
	"strings"
)

func Execute(done <-chan interface{}, messages <-chan string) <-chan string {
	results := make(chan string)

	go func() {
		defer close(results)

		for msg := range messages {
			select {
			case <-done:
				return
			default:
			}

			user, m := parser.ParsePrivmsg(msg)
			msgBreakdown := strings.Split(m, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0][1:]))

			if cmd == "colors" {
				results <- "https://gist.github.com/dylanaraps/3c688fe1db75838e52275264182b1ead"
				continue
			}

			if cmd == "color" {
				color := strings.TrimSpace(strings.Join(strings.Split(m, " ")[1:], " "))
				fmt.Println("User: ", user, "| Choose Color: ", color)
				changeColorScheme(color)
			}

		}
	}()

	return results
}

func changeColorScheme(color string) {
	if color == "" {
		fmt.Println("Color: ", "random_dark")
		exec.Command("./colorscheme.sh", "random_dark").Output()
		return
	}

	fmt.Println("Color: ", color)
	exec.Command("./colorscheme.sh", color).Output()
}
