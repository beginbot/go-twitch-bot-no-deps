package reporter

import (
	"fmt"
	"gitlab.com/beginbot/stylist/config"
	"gitlab.com/beginbot/stylist/irc"
)

//  This will need to take the config
func Report(done <-chan interface{}, messages <-chan string, c *config.Config) {
	go func() {
		for msg := range messages {
			select {
			case <-done:
				return
			default:
				irc.SendMsg(c, msg)
				fmt.Println(msg)
			}
		}
	}()
}
