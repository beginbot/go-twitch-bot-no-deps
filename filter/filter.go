package filter

import (
	"gitlab.com/beginbot/stylist/parser"
)

func Filter(done <-chan interface{}, messages <-chan string) <-chan string {
	userMsgs := make(chan string)

	go func() {
		defer close(userMsgs)

		for msg := range messages {
			select {
			case <-done:
				return
			default:
				if parser.IsPrivmsg(msg) {
					userMsgs <- msg
				}
			}
		}
	}()

	return userMsgs
}
